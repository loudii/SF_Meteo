<?php

namespace App\Utils;

class Utils{
    public static function getTrueWW(?int $ww) {
        switch (true) {
            case in_array($ww, range(0,19)):
                return 'sunny.png';
            case in_array($ww, range(20,29)):
                return 'clouds-and-sun.png';
            case in_array($ww, range(30,39)):
                return 'weather-1.png';
            case in_array($ww, range(40,49)):
                return 'morning-snow.png';
            case in_array($ww, range(50,59)):
                return 'tide-1.png';
            case in_array($ww, range(60,69)):
                return 'raining.png';
            case in_array($ww, range(70,79)):
                return 'raining.png';
            case in_array($ww, range(80,94)):
                return 'summer-rain.png';
            case in_array($ww, range(95,99)):
                return 'storm-2.png';
        }
    }

}

